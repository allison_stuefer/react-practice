# react-practice
A collection of random react apps completed during practice and learning from various resources.


## Planet Cards
A small, quick practice app as part of the Treehouse "Learn React" track. Render simple data.

## Star Rating
A small practice app as part of the Treehouse "Learn React" track. Update state on user input.

## Scoreboard
A simple scoreboard component that allows you to update a score of a predefinied player list. It is using the CDN script tags.

## Scoreboard.2
Taking the original Scoreboard project one step further by implementing build tools and modularizing the components. Running `npm start` in this directory will allow you to view the app in browser. 


