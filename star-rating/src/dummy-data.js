const sweets = [
  {
    id: '1',
    name: 'Pudding',
    desc: 'Cake fruitcake wafer sugar plum caramels chupa chups. Liquorice pastry jelly beans danish ice cream.',
    url: '/img/2.jpg'
  },
  {
    id: '2',
    name: 'Dessert',
    desc: 'Dragée brownie bonbon dessert bonbon powder.',
    url: '/img/3.jpg'
  },
  {
    id: '3',
    name: 'Tootsie Roll',
    desc: 'Bear claw dessert bonbon carrot cake fruitcake gingerbread.',
    url: '/img/5.jpg'
  },
  {
    id: '4',
    name: 'Donut',
    desc: 'Sweet roll candy canes macaroon donut donut topping.',
    url: '/img/6.jpg'
  },
  {
    id: '5',
    name: 'Fruitcake',
    desc: 'Tiramisu lollipop tootsie roll marzipan liquorice. Fruitcake sesame snaps fruitcake croissant toffee jelly-o marshmallow sesame snaps caramels.',
    url: '/img/7.jpg'
  },
  {
    id: '6',
    name: 'Tiramisu',
    desc: 'Topping carrot cake pastry caramels pastry jelly donut.',
    url: '/img/9.jpg'
  }
];

export default sweets;
