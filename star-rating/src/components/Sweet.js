import React from 'react';
import Rating from './Rating';

export const Sweet = (props) => {
  return (
    <div className="card">
      <div>
        <img src={props.url} alt={props.name} />
      </div>
      <h2>{ props.name }</h2>
      <p>{ props.desc }</p>
      <h3>Rating</h3>
      <Rating />
    </div>
  );
}
