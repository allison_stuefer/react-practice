import React from 'react';
import './App.css';
import {Sweet} from './components/Sweet';

export const App = (props) => {
  return (
    <div className="container">
      {props.sweets.map( sweet =>
        <Sweet
          {...sweet}
          key={sweet.id}
        />
      )}
    </div>
  );
}
